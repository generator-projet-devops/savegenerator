import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { CharacterService } from './character.service';
import { CreateCharacterDto } from './dto/create-character.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Character } from './schema/character.schema';

@ApiTags('Character')
@Controller('character')
export class CharacterController {
  constructor(private readonly characterService: CharacterService) {}

  @Post()
  @ApiOperation({ summary: 'Create a Character  ' })
  @ApiResponse({
    status: 201,
    description: 'This is your new Crew',
    type: Character,
  })
  create(@Body() createCharacterDto: CreateCharacterDto) {
    return this.characterService.create(createCharacterDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all characters' })
  @ApiResponse({
    status: 200,
    description: 'Get the information of all crew ',
    type: [Character],
  })
  findAll() {
    return this.characterService.findAll();
  }
  @Delete(':id')
  @ApiOperation({ summary: 'Delete a character by ID' })
  @ApiResponse({
    status: 200,
    description: 'Character deleted successfully',
    type: Character,
  })
  delete(@Param('id') id: string) {
    return this.characterService.delete(id);
  }
}
