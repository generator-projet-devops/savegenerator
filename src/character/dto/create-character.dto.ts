import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCharacterDto {
  @ApiProperty({ description: 'The name of the character' })
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiProperty({ description: 'The description of the character' })
  @IsNotEmpty()
  @IsString()
  readonly description: string;

  @ApiProperty({ description: 'The URL of the character' })
  @IsNotEmpty()
  @IsString()
  readonly url: string;
}
